import Vue from 'vue'
import VueRouter from 'vue-router'
import test from '../views/test.vue'
import login from '../views/login.vue'
import menu from '../views/menu.vue'
import orderlist from '../views/orderlist/orderlist.vue'
import slidesShow from '../views/orderlist/slidesShow.vue'
import userretrieve from '../views/userretrieve.vue'
import user from '../views/user.vue'
import diylist from '../views/orderlist/diylist.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: test,
    children: [
      {
        path: '/menu',
        name: 'menu',
        component: menu
      },
      {
        path: '/orderlist',
        name: 'orderlist',
        component: orderlist
      }

      
      ,
      {
        path: '/diylist',
        name: 'diylist',
        component: diylist
      },
      {
        path: '/slidesShow',
        name: 'slidesShow',
        component: slidesShow
      },
      {
        path: '/user',
        name: 'user',
        component: user
      },
      {
        path: '/userretrieve',
        name: 'userretrieve',
        component: userretrieve
      },
      {
       path: '/commodity',
       name: 'commodity',
       component: () => import(/* webpackChunkName: "about" */ '../views/commodity.vue')
      },
      {
        path: '/dynamicState',
        name: 'dynamicState',
        component: () => import(/* webpackChunkName: "about" */ '../views/DynamicAudit/dynamicState.vue')
       },
       {
        path: '/Customer',
        name: 'Customer',
        component: () => import(/* webpackChunkName: "about" */ '@/views/orderlist/Customer.vue')
       },
      {
        path: '/seckill',
        name: 'seckill',
        component: () => import(/* webpackChunkName: "about" */ '../views/seckill.vue')
       },
      {
       path: '/type',
       name: 'type',
       component: () => import(/* webpackChunkName: "about" */ '../views/type.vue')
      },
      {
        path: '/shang', 
        name: 'shang',
        component: () => import(/* webpackChunkName: "about" */ '../views/shang.vue')
       },
       {
        path: '/huishou',
        name: 'huishou',
        component: () => import(/* webpackChunkName: "about" */ '../views/huishou.vue')
       },
       {
        path:'/dynamicAudit',
        name:'dynamicAudit',
        component: () => import(/* webpackChunkName: "about" */ '../views/DynamicAudit/DynamicAudit.vue')
      }
    ]
   
  },
  {
    path: '/',
    name: 'login',
    component: login
  }
]

const router = new VueRouter({
  routes
})
export default router