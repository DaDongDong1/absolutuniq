import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import Fragment from 'vue-fragment'
import BaiduMap from 'vue-baidu-map'
Vue.use(Fragment.Plugin)
Vue.config.productionTip = false
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 */
  ak: 'UjFzxlrlPt4QWK3R3G3iKOLB0xtjFCx0'
})
// const http = axios.create({
//   baseURL:'http://101.43.223.224:19898',
// })
const http = axios.create({
  baseURL:'http://localhost:19898',
})
http.interceptors.request.use(config => {
  const token = window.localStorage.getItem('Authorization')
  if (token) {
    config.headers = { Authorization: token }
  }
  return config
})
http.interceptors.response.use(res => {
  if(res.data.code==20003){
    ElementUI.Message('请登录');
    router.push('/')
   }
 if(res.data.data&&res.data.data.errCode!=undefined && res.data.data.errCode==20011){
  ElementUI.Message('请重新登录');
  window.localStorage.setItem('Authorization',null)
  router.push('/')
 }
  return res
})
router.beforeEach((to, from, next) => {
  if (to.name != 'login') {
    if (window.localStorage.getItem('Authorization')) {
      next()
    } else {
      next('/')
    }
  }
  next()
})
Vue.directive('power',{
  inserted: function (el, binding, vnode) {
    let functionCode = vnode.data.attrs.functionCode;
    if (!functionCode) {
        return;
    }
    var aa = JSON.parse(window.localStorage.getItem('power'))[functionCode]
    console.log(aa)
    if(aa == undefined){
      el.parentNode.removeChild(el);
    return


    }
    let bindValue = binding.value;
    if (Object.prototype.toString.call(bindValue) === '[object Boolean]') {
        if (!bindValue) {
            el.parentNode.removeChild(el);
            return
        }
    }
  }
})
Vue.prototype.$axios = http
Vue.use(ElementUI)
new Vue({
  router,
  store,
 
  render: h => h(App)
}).$mount('#app')
